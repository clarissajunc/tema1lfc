#pragma once
#include <string>
class Productie
{
private:
	std::string m_stg;
	std::string m_dr;

public:
	Productie(std::string, std::string);

	friend std::ostream& operator<<(std::ostream&, const Productie&);

	std::string GetStanga();
	std::string GetDreapta();

};

