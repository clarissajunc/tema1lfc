#include "Gramatica.h"
#include<fstream>
#include <iostream>
#include<ctime>
#include<random>

void Gramatica::citire()
{
	std::ifstream input("Gramatica.txt");
	int lung;
	input >> lung;
	char aux;
	for (int i = 0; i < lung; i++)
	{
		input >> aux;
		neterminale.insert(aux);
	}
	input >> lung;
	for (int i = 0; i < lung; i++)
	{
		input >> aux;
		terminale.insert(aux);
	}
	input >> s;
	input >> lung;
	for (int i = 0; i < lung; i++)
	{
		std::string stanga, dreapta;
		input >> stanga >> dreapta;
		productii.emplace_back(Productie(stanga, dreapta));
	}

}

bool Gramatica::verificare()
{
	//verificare multimi disjuncte
	for (auto i : neterminale)
	{
		for (auto j : terminale)
			if (i == j)
			{
				std::cout << "Regula 1.";
				return false;
			}
	}

	//verificare s in vn
	bool okB = false;
	for (auto it : neterminale)
		if (it == s)
			okB = true;
	if (okB == false)
	{
		std::cout << "Regula 2.";
		return false;
	}

	//verificare pt fiecare regula stg contine un neterminal
	for (auto i : productii)
	{
		int ok1 = -1;
		for (auto j : neterminale)
		{
			ok1 = i.GetStanga().find(j);
			if (ok1 != -1)
				break;
		}
		if (ok1 == -1)
		{
			std::cout << "Regula 3.";
			return false;
		}
	}

	//verificare cel putin o prod cu s in stg
	int k = -1;
	for (auto it : productii)
	{
		if (it.GetStanga().size() == 1 && it.GetStanga()[0] == s)
		{
			k = 1;
			break;
		}
	}
	if (k == -1)
	{
		std::cout << "Regula 4.";
		return false;
	}

	//verificare fiecare prod elem din vn si vt
	for (auto it : productii)
	{
		auto ok = neterminale.begin();
		for (auto i : it.GetStanga())
		{
			ok = neterminale.find(i);
			if (ok == neterminale.end())
			{
				ok = terminale.find(i);
				if (ok == terminale.end())
				{
					std::cout << "Regula 5.";
					return false;
				}
			}
		}
		for (auto i : it.GetDreapta())
		{
			ok = neterminale.find(i);
			if (ok == neterminale.end())
			{
				ok = terminale.find(i);
				if (ok == terminale.end())
				{
					std::cout << "Regula 5.";
					return false;
				}
			}
		}
	}
	return true;
}

void Gramatica::afisare()
{
	std::cout << "Vneterminale: ";
	for (auto it : neterminale)
		std::cout << it << " ";
	std::cout << std::endl;
	std::cout << "Vterminale: ";
	for (auto it : terminale)
		std::cout << it << " ";
	std::cout << std::endl;
	std::cout << "Simbolul de start: " << s << std::endl;
	std::cout << "Productii: " << std::endl;
	for (auto it : productii)
	{
		std::cout << it.GetStanga() << "->" << it.GetDreapta();
		std::cout << std::endl;
	}
}

void Gramatica::generare(bool optiune, int n)
{
	srand(time(0));
	for (int i = 0; i < n; i++)
	{

		std::string cuvant;
		std::vector<bool> aplicabile(productii.size(), 0);
		cuvant.push_back(s);
		std::cout << "Cuvant nou: "<<cuvant;
		bool existaProd = 1;
		while (existaProd)
		{
			existaProd = 0;
			int index = 0;
			for (auto it : productii)
			{
				auto pos = cuvant.find(it.GetStanga());
				if (pos != -1)
				{
					aplicabile[index] = 1;
					existaProd = 1;
				}
				index++;
			}
			if (existaProd == 0)
			{
				int ok123 = 0;
				for (auto it : cuvant)
				{
					auto ok = terminale.find(it);
					if (ok == terminale.end())
					{
						std::cout << " Cuvantul " << cuvant << " nu apartine limbajului"<<std::endl;
						ok123 = 1;
						break;
					}
				}
				if (ok123 == 0)
				{
					std::cout <<  " Cuvantul " << cuvant << " apartine limbajului."<<std::endl;
				}
				break;
			}
			int alegere = rand() % (aplicabile.size());
			while (aplicabile[alegere] == 0)
			{
				alegere = rand() % (aplicabile.size());
			}
			int pos = cuvant.find(productii[alegere].GetStanga());
			cuvant.replace(pos, productii[alegere].GetStanga().size(), productii[alegere].GetDreapta());
			if (optiune)
				std::cout << "->" << "(" << alegere << ")" << cuvant;
			std::fill(aplicabile.begin(), aplicabile.end(), 0);
		}
	}
}
