#include "Productie.h"
#include <iostream>

Productie::Productie(std::string stg, std::string dr): m_stg(stg), m_dr(dr)
{
	//empty
}

std::string Productie::GetStanga()
{
	return this->m_stg;
}

std::string Productie::GetDreapta()
{
	return this->m_dr;
}

std::ostream& operator<<(std::ostream& out, const Productie& productie)
{
	
	out << productie.m_stg<<"->";
	out << productie.m_dr;
	return out;
}


