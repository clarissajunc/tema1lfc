#pragma once
#include<vector>
#include<set>
#include "Productie.h"
class Gramatica
{
private:
	std::set<char> neterminale;
	std::set<char> terminale;
	char s;
	std::vector<Productie> productii;

public:
	void citire();
	bool verificare();
	void afisare();
	void generare(bool, int);
};

